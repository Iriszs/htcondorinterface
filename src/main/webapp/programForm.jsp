<%-- 
    Copyright (c) 2017 Maaike Brummer & Iris Gorter
    All rights reserved
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="css/my_css.css">
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <title>MI Condor</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="condorinterface-jobsubmit.js"></script>
    </head>
    <body class="formbody">
        <h1 class="form-title">Start a new run!</h1>

        <div class="container">
            <div class="row formLinks">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form class="formLinks" id="formLinks">
                        <c:choose>
                            <c:when test="${sessionScope.ExecutablePathError == 1}">
                                <div class="alert alert-danger"><b>ERROR: </b>Your given program file is not correct. Please try again.</div>
                            </c:when>    
                            <c:otherwise>
                                &nbsp;
                            </c:otherwise>
                        </c:choose>

                        <div class="form-group">
                            <label>Program: </label>
                            <input type="text" name="program" placeholder="/usr/bin/blastn" required class="form-control">

                        </div>
                        <div class="form-group">
                            <label>Arguments: </label>
                            <input type="text" name="arguments" required placeholder="-word_size 4" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Run name: </label>
                            <input type="text" name="runname" required class="form-control">
                        </div>                                
                        <div class="form-group">
                            <label>Queue: </label>
                            <input type="number" name="queue" min="1" max="15" required class="form-control">
                        </div>                                

                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <input id="btnSearch" type="button" value="Send job to Condor!" onclick="getJobData()" class="btn btn-success btn-md center-block">
                                <input id="btnClear" type="reset" class="btn btn-success btn-md center-block">
                            </div>
                        </div>

                        <c:choose>
                            <c:when test="${sessionScope.inputerror == 1}">
                                <div class="alert alert-danger"><b>ERROR: </b>There is something wrong, your condor job does not start.</div>
                                <%
                                    String valueofString = request.getSession().getAttribute("errorFile").toString();
                                    out.write(valueofString);
                                %>
                            </c:when>
                            <c:otherwise>
                                &nbsp;
                            </c:otherwise>
                        </c:choose>

                        <c:choose>
                            <c:when test="${sessionScope.idleError == 1}">
                                <div class="alert alert-danger"><b>ERROR: </b>Your job is Idle for more than 5 seconds. Check with condor_q in the terminal if your job is still idle or not.</div>
                            </c:when>
                            <c:otherwise>
                                &nbsp;
                            </c:otherwise>
                        </c:choose>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>

<%-- 
    Copyright (c) 2017 Maaike Brummer & Iris Gorter
    All rights reserved
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="nl.bioinf.imgorter.condorInterface.model.Arguments"%>
<%@page import="nl.bioinf.imgorter.condorInterface.model.JobExtractor"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="css/my_css.css">
<!DOCTYPE html>
<html>
    <head>
        <title>MI Condor</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </head>
    <body class="formbody">
        <h1 class="form-title">MI Condor</h1>

        <div class="container">
            <div class="row formLinks">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form action="Refresh.do" method="POST">
                        <div class="form-group">
                            <input type="submit" value="Refresh jobs and cores" name="runningjobs" class="btn btn-primary center-block"/>                            
                        </div>
                    </form>

                    <form action="KillJobs.do" method="POST" class="formLinks"> 
                        <div class="form-group">
                            <label>Check your current running jobs:</label>
                            <%if (JobExtractor.ExtractJobs() != null) {
                                    ArrayList<String> t = JobExtractor.ExtractJobs();
                                    pageContext.setAttribute("jobs", t);
                                    String runningJobs = "";
                                    for (String running : t) {
                                        if (runningJobs == "") {
                                            runningJobs = running;
                                        } else {
                                            runningJobs = runningJobs + ", " + running;
                                        }
                                    }
                                    request.getSession().setAttribute("running", runningJobs);
                                }%>
                            <h4>${sessionScope.running}</h4>
                        </div>

                        <script>
                            $("#test").ready(function () {
                                $("p").hide();
                                $(".killjobs").click(function () {
                                    $("p").show();
                                });
                            });
                        </script>

                        <div class="form-group col-sm-12">
                            <input type="submit" value="Kill running jobs" name="killJob" class="killjobs btn btn-danger btn-md"/> 
                        </div>
                        <p id="test" class="btn-md">Jobs killed.</p>

                        <div class="form-group">
                            <label>Check the current available cores:</label>

                            <%ArrayList<String> cores = JobExtractor.ExtractCores();
                                pageContext.setAttribute("cores", cores);%>

                            <%
                                String xytabel = "";
                                String elementen1 = "";
                                String elementen2 = "";
                                for (String core : cores) {
                                    if (core.contains("Total Owner Claimed Unclaimed Matched Preempting Backfill")) {
                                        xytabel = "<table class='table table-bordered'>"
                                                + "<tr><th>" + ""
                                                + "</th><th>" + core.split("\\s+")[1]
                                                + "</th><th>" + core.split("\\s+")[2]
                                                + "</th><th>" + core.split("\\s+")[3]
                                                + "</th><th>" + core.split("\\s+")[4]
                                                + "</th><th>" + core.split("\\s+")[5]
                                                + "</th><th>" + core.split("\\s+")[6]
                                                + "</th><th>" + core.split("\\s+")[7] + "</th></tr>";

                                    } else if (core.contains("X86_64/LINUX")) {
                                        elementen1
                                                = "<tr><td>" + core.split("\\s+")[1].replaceAll("\\s+", "") + "</td>"
                                                + "<td>" + core.split("\\s+")[2].replaceAll("\\s+", "") + "</td>"
                                                + "<td>" + core.split("\\s+")[3].replaceAll("\\s+", "") + "</td>"
                                                + "<td>" + core.split("\\s+")[4].replaceAll("\\s+", "") + "</td>"
                                                + "<td>" + core.split("\\s+")[5].replaceAll("\\s+", "") + "</td>"
                                                + "<td>" + core.split("\\s+")[6].replaceAll("\\s+", "") + "</td>"
                                                + "<td>" + core.split("\\s+")[7].replaceAll("\\s+", "") + "</td>"
                                                + "<td>" + core.split("\\s+")[8].replaceAll("\\s+", "") + "</td></tr>";

                                    } else if (core.contains("Total")) {
                                        elementen2
                                                = "<tr><td>" + core.split("\\s+")[1].replaceAll("\\s+", "") + "</td>"
                                                + "<td>" + core.split("\\s+")[2].replaceAll("\\s+", "") + "</td>"
                                                + "<td>" + core.split("\\s+")[3].replaceAll("\\s+", "") + "</td>"
                                                + "<td>" + core.split("\\s+")[4].replaceAll("\\s+", "") + "</td>"
                                                + "<td>" + core.split("\\s+")[5].replaceAll("\\s+", "") + "</td>"
                                                + "<td>" + core.split("\\s+")[6].replaceAll("\\s+", "") + "</td>"
                                                + "<td>" + core.split("\\s+")[7].replaceAll("\\s+", "") + "</td>"
                                                + "<td>" + core.split("\\s+")[8].replaceAll("\\s+", "") + "</td></tr></table>";
                                    }
                                }

                                String tabel = xytabel + elementen1 + elementen2;
                                out.println(tabel);

                            %>

                        </div>
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <p>Before you get started, make sure to delete unnecessary files in your tmp folder once in a while!</p>
                                <input id="btnSearch" type="button" value="Start New BLAST Job!" name="newJob" onclick="openPage('form.jsp')" class="btn btn-success btn-md center-block"/>
                                <input id="btnClear" type="button" value="Start A New Job!" name="newJob" onclick="openPage('programForm.jsp')" class="btn btn-success btn-md center-block"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            function openPage(pageURL) {
                window.location.href = pageURL;
            }
        </script>
    </body>
</html>

/* 
 * Copyright (c) 2017 Maaike Brummer & Iris Gorter
 * All rights reserved
 */
function getJobData() {
    var form = $("#formLinks");
    // Get input files
    var data = {
        program: form.find("input[name=program]").val(),
        runname: form.find("input[name=runname]").val(),
        queue: form.find("input[name=queue]").val(),
        arguments: form.find("input[name=arguments]").val()
    };
    
    // Create dummy form
    var dummy = document.createElement("form");
    dummy.method = "POST";
    dummy.action = "/condorInterface/submitJob.do";
    dummy.id = "dummyForm";
    document.body.appendChild(dummy);
    
    // Create new input
    var element = document.createElement('input');
    element.type = "text";
    element.name = "formargs2";
    element.id = "dummyInput";
    
    // Bind to dummy form
    dummy = document.getElementById("dummyForm");
    dummy.appendChild(element);
    
    $("#dummyInput").val(JSON.stringify(data));
    $("#dummyForm").submit();
}






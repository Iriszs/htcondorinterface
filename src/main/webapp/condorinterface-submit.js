/* 
 * Copyright (c) 2017 Maaike Brummer & Iris Gorter
 * All rights reserved
 */
function getData() {
    var form = $("#condorform");
    // Get input files
    var data = {
        program: form.find("select[name=program]").val(),
        inputfilepath: form.find("input[name=inputfilepath]").val(),
        outputfile: form.find("input[name=outputfile]").val(),
        database: form.find("select[name=database]").val(),
        runname: form.find("input[name=runname]").val(),
        queue: form.find("input[name=queue]").val(),
        inputfile: []
    };
    
    for (var i = 0; i < $("input[name=inputfile]").get(0).files.length; ++i) {
        data.inputfile.push($("input[name=inputfile]").get(0).files[i].name);
    }
    
    // Create dummy form
    var dummy = document.createElement("form");
    dummy.method = "POST";
    dummy.action = "/condorInterface/submitBlast.do";
    dummy.id = "dummyForm";
    document.body.appendChild(dummy);
    
    // Create new input
    var element = document.createElement('input');
    element.type = "text";
    element.name = "formargs";
    element.id = "dummyInput";
    
    // Bind to dummy form
    dummy = document.getElementById("dummyForm");
    dummy.appendChild(element);
    
    $("#dummyInput").val(JSON.stringify(data));
    $("#dummyForm").submit();
}

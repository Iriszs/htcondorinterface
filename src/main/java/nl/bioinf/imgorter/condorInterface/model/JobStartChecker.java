/*
 * Copyright (c) 2017 Maaike Brummer & Iris Gorter
 * All rights reserved
 */
package nl.bioinf.imgorter.condorInterface.model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * this is a class that checks if a job that has been send to condor really
 * starts running. It contains a method that checks if the job starts. If the
 * jobs starts, the user will be send to the home page. If not, the user will
 * see the error log of condor.
 */
public class JobStartChecker {
    Arguments arg = new Arguments();
    private int inputerror = 0;
    private int idleError = 0;
    
    int jobsNulWaarde = 0;
    int idleNulWaarde = 0;
    int runningNulWaarde = 0;
    
    int jobsSecond = 0;
    int idleSecond = 0;
    int runningSecond = 0;

    public JobStartChecker() {
        
    }

     /**
     * This method calls itself with the arguments that are extracted from the 
     * form. 
     * @param arguments 
     */
    public JobStartChecker(Arguments arguments) {
        arg = arguments;
    }

    /**
     * Getter for inputerror. Inputerror is by default 0. When there is 
     * something wrong with the inputerror, the error will be set to 1.
     * @return inputerror 
     */
    public int getInputerror() {
        return inputerror;
    }

     /**
     * Setter for inputerror. Inputerror is by default 0. When there is 
     * something wrong with the inputerror, the error will be set to 1.
     * @param inputerror 
     */
    public void setInputerror(int inputerror) {
        this.inputerror = inputerror;
    }
    
    /**
     * Getter for the idleError. Idleerror is by default 0. When a job does not
     * start to run, but it also does not give an error, it is idle. IdleError
     * will be set to 1 when a new job goes into idle. The user then has to
     * check for themselves if the job actually starts running
     * @return idleError
     */    
    public int getIdleError() {
        return idleError;
    }

     /**
     * Setter for the idleError. Idleerror is by default 0. When a job does not
     * start to run, but it also does not give an error, it is idle. IdleError
     * will be set to 1 when a new job goes into idle. The user then has to
     * check for themselves if the job actually starts running
     * @param idleError 
     */
    public void setIdleError(int idleError) {
        this.idleError = idleError;
    }


/**
 * This method checks if a job really starts after submitting.
 * When this method is called, it determines the number of jobs jobsFirst before
 * a new job is started. After the submit button is hit, it will determine the
 * number of jobs again (jobsSecond). After 5 seconds, it determines the number
 * of jobs again (jobsThird). This is done because almost every BLAST job starts
 * but when a database is not correct in usage with the program (a protein db 
 * with a blastn doesn't work) it starts to run for like 2-3 seconds and then
 * stops. This is why there is a check after 5 seconds. If the number of 
 * jobsFirst is the same as jobSecond, the job didn't start. If the number of
 * jobsSecond is bigger than jobsThird, the job quit probably because of an
 * error. When jobsSecond is bigger than jobsFirst and is smaller or the same
 * as jobsThird, the job started.
 * @throws IOException 
 */
    public String checkStart(String chosenJob) throws IOException{
        System.out.println("*********   START    **********");
        //get the values before executing
        this.getNulwaarden();

        //create new condor.cfg file with entered values in the form
        FileCreator fc = new FileCreator(arg);
        fc.createFile(chosenJob);

        try {
            //execute the newly created condor.cfg file
            CondorExecuter ce = new CondorExecuter();
            ce.executeCommands();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        
        try {
            //wait 10 seconds
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        //get the new values
        this.getSecondWaarden();

        if (jobsNulWaarde == jobsSecond) {
            //total jobs did not rise, so it did not start
            System.out.println("Error 1 Inputerror");
             setInputerror(1);
             return getErrorMessage();
        } else if (jobsNulWaarde < jobsSecond) {
            //there are new total jobs, but they may be idle
            //idle values did not rise, so the new jobs are not idle
            if(idleNulWaarde == idleSecond){
                //running jobs did rise, so the new jobs are running
                if(runningNulWaarde<runningSecond){
                     setInputerror(0);
                } else if (runningNulWaarde == runningSecond) {
                    //running jobs did not rise, so no new running jobs
                    System.out.println("Error 2 Inputerror");
                    setInputerror(1);
                    return getErrorMessage();
                }
            } else if (idleNulWaarde<idleSecond) {
                //jobs are idle, user has to check for themselves
                System.out.println("Idle error");
                setIdleError(1);
            }
        }
        return "";
    }

    /**
     * This method extracts the values of the total jobs, the running and idle
     * after waiting 10 seconds.
     * @throws IOException 
     */    
    public void getSecondWaarden() throws IOException{
        JobExtractor jeSecond = new JobExtractor();
        ArrayList<String> naCreate = jeSecond.ExtractJobs();
        for(String nulwaarden : naCreate){
            if (nulwaarden.contains("jobs")){
                System.out.println("SecondWaarde jobs: " + nulwaarden);
                jobsSecond = Integer.parseInt(nulwaarden.split(" ")[0]);
            }
            if (nulwaarden.contains("idle")) {
                System.out.println("SecondWaarde idle: " + nulwaarden);
                idleSecond = Integer.parseInt(nulwaarden.split(" ")[0]);
            }
            if (nulwaarden.contains("running")) {
                System.out.println("SecondWaarde running: " + nulwaarden);
                runningSecond = Integer.parseInt(nulwaarden.split(" ")[0]);
            }
        }
    }
    
    /**
     * This method extracts the values of total jobs, the running and idle 
     * before submitting the file.
     * @throws IOException 
     */   
    public void getNulwaarden() throws IOException{
        JobExtractor jeFirst = new JobExtractor();
        ArrayList<String> voorCreate = jeFirst.ExtractJobs();
        for(String nulwaarden : voorCreate){
            if (nulwaarden.contains("jobs")){
                System.out.println("nulwaarde jobs: " + nulwaarden);
                jobsNulWaarde = Integer.parseInt(nulwaarden.split(" ")[0]);
            }
            if (nulwaarden.contains("idle")) {
                System.out.println("nulwaarde idle: " + nulwaarden);
                idleNulWaarde = Integer.parseInt(nulwaarden.split(" ")[0]);
            }
            if (nulwaarden.contains("running")) {
                System.out.println("nulwaarde running: " + nulwaarden);
                runningNulWaarde = Integer.parseInt(nulwaarden.split(" ")[0]);
            }
        }
    }

     /**
     * This method returns the errormessage from a condor.error file
     * @return string
     */
    public String getErrorMessage() {
        UsernameGetter ug = new UsernameGetter();
        String username;
        StringBuffer buf = new StringBuffer();
        try {
            username = ug.getUsername();
            //read the errorfile from the user
            BufferedReader reader = new BufferedReader(new FileReader(arg.getError().replace("userName", username)));
            String line = "";
            while ((line = reader.readLine()) != null) {
                buf.append(line);
            }
            reader.close();

        } catch (FileNotFoundException fnf) {
            fnf.printStackTrace();
        } catch (IOException io) {
            io.printStackTrace();
        } catch (InterruptedException i) {
            i.printStackTrace();
        }
        return buf.toString();
    }
}

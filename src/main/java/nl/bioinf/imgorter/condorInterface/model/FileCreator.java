/*
 * Copyright (c) 2017 Maaike Brummer & Iris Gorter
 * All rights reserved
 */
package nl.bioinf.imgorter.condorInterface.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Class that generates cfg file from information of the form.
 */
public class FileCreator {

    private Arguments arg = new Arguments();

    FileCreator() {

    }

    public FileCreator(final Arguments newA) {
        arg = newA;
    }

    /**
     * Method that creates the outputfile (cfg) from the given arguments in the
     * form.
     * @exception IOException, IterruptedException
     */
    public void createFile(String chosenJob) {
        //Get username from class UsernameGetter().
        UsernameGetter ug = new UsernameGetter();
        String username = "";
        try {
            username = ug.getUsername();
        } catch (IOException io) {
            io.printStackTrace();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        String f = arg.getDemoFile();
        File log = new File(f);

        try {
            BufferedReader reader = new BufferedReader(new FileReader(log));
            String line = "";
            String oldtext = "";
            while ((line = reader.readLine()) != null) {
                oldtext += line + "\r\n";
            }

            reader.close();
            if (chosenJob == "BLAST") {
                FileWriter writer = new FileWriter(("/homes/userName/tmp/condor.cfg").replace("userName", username));
                for (String q : arg.getQuery().split(" ")) {
                    String last = q.substring(q.lastIndexOf('/') + 1);
                    String baseName = last.split("\\.")[0];

                    String result = oldtext.replaceAll("§UNIVERSE§", "vanilla")
                            .replaceAll("§LOG§", arg.getLog().replace("userName", username).replace("condor", arg.getRunname() + baseName))
                            .replaceAll("§ERROR§", arg.getError().replace("userName", username).replace("condor", arg.getRunname() + baseName))
                            .replaceAll("§OUTPUT§", arg.getOutput().replace("userName", username).replace("condor", arg.getRunname() + baseName))
                            .replaceAll("§QUEUE§", Integer.toString(arg.getQueue()))
                            .replaceAll("§EXECUTABLE§", arg.getExecutable())
                            .replaceAll("§ARGUMENTS§", ("-query " + q
                                    + " -db " + "/data/datasets/" + arg.getDb()
                                    + " -out " + arg.getOut()))
                            .replaceAll("§NOTIFICATION§", "never");
                    writer.write(result);

                }
                System.out.println("File is aangemaakt en regels toegevoegd");
                writer.close();

            } else if (chosenJob == "REGULAR") {
                FileWriter writer = new FileWriter(("/homes/userName/tmp/condor.cfg").replace("userName", username));
                //toevoegen van bestaande dingen. 
                String result = oldtext.replaceAll("§UNIVERSE§", "vanilla")
                        .replaceAll("§LOG§", arg.getLog().replace("userName", username).replace("condor", arg.getRunname()))
                        .replaceAll("§ERROR§", arg.getError().replace("userName", username).replace("condor", arg.getRunname()))
                        .replaceAll("§OUTPUT§", arg.getOutput().replace("userName", username).replace("condor", arg.getRunname()))
                        .replaceAll("§ARGUMENTS§", arg.getArguments())
                        .replaceAll("§QUEUE§", Integer.toString(arg.getQueue()))
                        .replaceAll("§EXECUTABLE§", arg.getExecutable())
                        .replaceAll("§NOTIFICATION§", "never");
                writer.write(result);
                System.out.println("File is aangemaakt en regels toegevoegd");
                writer.close();
            } else {
                System.out.println("Error");
            }
        } catch (IOException io) {
            io.printStackTrace();
        }
    }
}
/*
 * Copyright (c) 2017 Maaike Brummer & Iris Gorter
 * All rights reserved
 */
package nl.bioinf.imgorter.condorInterface.model;

import java.io.File;

/**
 * Class Arguments with getters and setters.
 * Getters and setters for all the arguments that are used througout the whole
 * web app.
 */
public class Arguments {
    private String jobs;
    private String running;
    private String idle;
    private String log = "/homes/userName/tmp/condor.log";
    private String error = "/homes/userName/tmp/condor.error";
    private String output = "/homes/userName/tmp/condor.out";
    private String executable;
    private String query;
    private String db;
    private String out;
    private int outputPathError = 0;
    private int executablePathError = 0;
    private String arguments;
    private int queue;
    private String dbPath;
    private String demoFile;
    private String runname;

    /**
     * @return runname (String)
     * Runname is the name of a condor run that the user gives in the form.
     */
    public String getRunname() {
        return runname;
    }

    /**
     * @param runname
     * Runname is the name of a condor run that the user gives in the form.
     */
    public void setRunname(final String runname) {
        this.runname = runname;
    }

    /**
     * @return jobs (String).
     * Jobs contains a number that is obtained from the 'jobs' variable from the
     * condor_q statement. It is the total number of all jobs (includes idle and
     * running).
     */
    public String getJobs() {
        return jobs;
    }

    /**
     * @param jobs (String)
     * Jobs contains a number that is obtained from the 'jobs' variable from the
     * condor_q statement. It is the total number of all jobs (includes idle and
     * running).
     */
    public void setJobs(final String jobs) {
        this.jobs = jobs;
    }

    /**
     * @return running (String).
     * Running is the number that is obtained from the 'running' variable from
     * the condor_q statement. It is the total number of the running jobs.
     */
    public String getRunning() {
        return running;
    }

    /**
     * @param running (String)
     * Running is the number that is obtained from the 'running' variable from
     * the condor_q statement. It is the total number of the running jobs.
     */
    public void setRunning(final String running) {
        this.running = running;
    }

    /**
     * @return idle (String).
     * Idle is the number that is obtained from the 'idle' variable from the
     * condor_q statement. It is the total number of idle jobs.
     */
    public String getIdle() {
        return idle;
    }

    /**
     * @param idle (String)
     * Idle is the number that is obtained from the 'idle' variable from the
     * condor_q statement. It is the total number of idle jobs.
     */
    public void setIdle(final String idle) {
        this.idle = idle;
    }

    /**
     * @return demoFile (String)
     * demofile is the path to the template cfg.
     */
    public String getDemoFile() {
        return demoFile;
    }

    /**
     * @param demoFile (String).
     * demofile is the path to the template cfg.
     */
    public void setDemoFile(final String demoFile) {
        this.demoFile = demoFile;
    }

    /**
     * Method to check if the program (executable) exists.
     * it checks if path is present and if the path+program exists,
     * if it does not exists, feedback in the form.
     * @param executable (String)
     * Executable is a string that contains a program or a program and a path
     */
    public void setExecutable(final String executable) {
        //if the user entered the program with the path
        if (new File(executable.toLowerCase()).exists()) {
            System.out.println("File bestaat");
            this.executable = executable.toLowerCase();
            this.executablePathError = 0;
            //if the user only entered the programname and not the path
        } else if (new File("/usr/bin/" + executable.toLowerCase()).exists()) {
            System.out.println("File bestaat met path ervoor");
            this.executable = "/usr/bin/" + executable.toLowerCase();
            this.executablePathError = 0;
            //if the user entered completely something else.
        } else {
            this.executablePathError = 1;
            System.out.println("Error");
        }
    }

    /**
     * @return executablePathError (int).
     * Executablepatherror is a number that an errormessage initiates or not.
     * The number is set to 1 if the entered program doesn't exist. If the
     * program exists, the number is 0. Is standard 0.
     */
    public int getExecutablePathError() {
        return executablePathError;
    }

    /**
     * @param executablePathError (int).
     * Executablepatherror is a number that an errormessage initiates or not.
     * The number is set to 1 if the entered program doesn't exist. If the
     * program exists, the number is 0.
     */
    public void setExecutablePathError(final int executablePathError) {
        this.executablePathError = executablePathError;
    }

    /**
     * @return executable (String).
     * Executable is a string that contains a program or a program and a path
     */
    public String getExecutable() {
        return executable;
    }
    /**
     * @return arguments (String).
     * Arguments is a string that contains every argument that is entered in
     * the form to start a normal job (any job besides condor)
     */
    public String getArguments() {
        return arguments;
    }

    /**
     * @param arguments (String).
     * Arguments is a string that contains every argument that is entered in
     * the form to start a normal job (any job besides condor)
     */
    public void setArguments(final String arguments) {
        this.arguments = arguments;
    }

    /**
     * @return outputPathError (int).
     * outputPathError is a number that indicates if there is an error with the
     * place where the outputfile is stored. Is 1 if there is an error and is 0
     * when there is none.
     */
    public int getOutputPathError() {
        return outputPathError;
    }

    /**
     * @param outputPathError (int).
     * outputPathError is a number that indicates if there is an error with the
     * place where the outputfile is stored. Is 1 if there is an error and is 0
     * when there is none. Is standard 0.
     */
    public void setOutputPathError(final int outputPathError) {
        this.outputPathError = outputPathError;
    }

    /**
     * @return out (String).
     * out is a string that contains the outputfilename with the path where it
     * is stored.
     */
    public String getOut() {
        return out;
    }

    /**
     * This method checks if the outputfile exists or not.
     * If the file doesn't exist, outputpatherror will be set to 1. If the file
     * does exist, the error will be set to 0.
     * @param out (String).
     * out is a string that contains the outputfilename with the path where it
     * is stored.
     */
    public void setOut(final String out) {
        if (!new File(out).exists()) {
            this.outputPathError = 1;
        } else {
            this.outputPathError = 0;
            this.out = out;
        }
    }

    /**
     * @return dbPath (String).
     * dbPath is a string that contains the place where the databases are stored
     */
    public String getDbPath() {
        return dbPath;
    }

    /**
     * @param dbPath (String).
     * dbPath is a string that contains the place where the databases are stored
     */
    public void setDbPath(final String dbPath) {
        this.dbPath = dbPath;
    }

    /**
     * @return query (String).
     * Query is a string that contains all the elements that should be placed
     * after query = in the condor configuration file (.cfg)
     */
    public String getQuery() {
        return query;
    }

    /**
     * @param query (String).
     * Query is a string that contains all the elements that should be placed
     * after query = in the condor configuration file (.cfg)
     */
    public void setQuery(final String query) {
        this.query = query;
    }

    /**
     * @return db (String).
     * db is a string that contains the name of the database that has been
     * selected in the form.
     */
    public String getDb() {
        return db;
    }

    /**
     * @param db (String).
     * db is a string that contains the name of the database that has been
     * selected in the form.
     */
    public void setDb(final String db) {
        this.db = db;
    }

    /**
     * @param log (String).
     * log is a string that contains the path where the log file should be
     * stored. Is used to replace the userName to the username from the user.
     */
    public void setLog(final String log) {
        this.log = log;
    }

    /**
     * @param error (String).
     * error is a string that contains the path where the error file should be
     * stored. Is used to replace the userName to the username from the user.
     */
    public void setError(final String error) {
        this.error = error;
    }

    /**
     * @param output (String).
     * out is a string that contains the path where the output file should be
     * stored. Is used to replace the userName to the username from the user.
     */
    public void setOutput(final String output) {
        this.output = output;
    }

    /**
     * @param queue (int).
     * Queue is an int that contains the number that is placed after the word
     * queue in the condor configuration file (.cfg)
     */
    public void setQueue(final int queue) {
        this.queue = queue;
    }

    /**
     * @return log (String).
     * log is a string that contains the path where the log file should be
     * stored. Is used to replace the userName to the username from the user.
     */
    public String getLog() {
        return log;
    }

    /**
     * @return error (String).
     * error is a string that contains the path where the log file should be
     * stored. Is used to replace the userName to the username from the user.
     */
    public String getError() {
        return error;
    }

    /**
     * @return output (String).
     * output is a string that contains the path where the log file should be
     * stored. Is used to replace the userName to the username from the user.
     */
    public String getOutput() {
        return output;
    }

    /**
     * @return queue (int).
     * Queue is an int that contains the number that is placed after the word
     * queue in the condor configuration file (.cfg)
     */
    public int getQueue() {
        return queue;
    }
}
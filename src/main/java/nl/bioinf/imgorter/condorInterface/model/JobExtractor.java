/*
 * Copyright (c) 2017 Maaike Brummer & Iris Gorter
 * All rights reserved
 */
package nl.bioinf.imgorter.condorInterface.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * Class to get the information of the (current) jobs.
 */
public class JobExtractor {

    public JobExtractor() {

    }

    /**
     * Method that executes the program jobs.sh to get the running jobs, 
     * it uses the method CheckJobs to get the amount of running jobs.
     * @return runningJobs
     * @throws IOException
     */
    public static ArrayList<String> ExtractJobs() throws IOException {
        //get output of the condor_q statement
        String[] command = {"condor_q"};
        ProcessBuilder probuilder = new ProcessBuilder(command);
        Process process = probuilder.start();
        InputStream is = process.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line;
        System.out.printf("Output of running %s is:\n", Arrays.toString(command));
        ArrayList<ArrayList<String>> runningJobs = new ArrayList<>();
        while ((line = br.readLine()) != null) {
            System.out.println("line: " + line);
            runningJobs.add(JobExtractor.CheckJobs(line));
        }
        return runningJobs.get(runningJobs.size() - 1);
    }
    
    /**
     * Method to check the amount of running jobs.
     * @return arg.getRunningJobs() (String)
     */
    public static ArrayList<String> CheckJobs(String line) {
        Arguments arg = new Arguments();
        //The output of condor_q contains a number, followed by an space and the string 'jobs'.
        System.out.println("line: " + line);
        String patternJobs = "[0-9]* jobs";
        String patternIdle = "[0-9]* idle";
        String patternRunning = "[0-9]* running";
        
        Pattern pJ = Pattern.compile(patternJobs);
        Pattern pI = Pattern.compile(patternIdle);
        Pattern pR = Pattern.compile(patternRunning);
        
        Matcher mJ = pJ.matcher(line);
        Matcher mI = pI.matcher(line);
        Matcher mR = pR.matcher(line);

        
        //if the pattern in the output of jobs.sh
        if (mJ.find() && mI.find() && mR.find()) {
            //get the number of jobs without the string jobs.
            arg.setJobs(mJ.group(0));
            arg.setIdle(mI.group(0));
            arg.setRunning(mR.group(0));
        } else {
            System.out.println("No match found");
        }
        ArrayList<String> r = new ArrayList<String>();
        r.add(arg.getJobs());
        r.add(arg.getIdle());
        r.add(arg.getRunning());
        return r;
    }

    /**
     * Method to extract the amount of cores. TODO
     * @return core_lines
     * @throws IOException
     */
    public static ArrayList<String> ExtractCores() throws IOException {
        // Command to get output of the condor_status statement
        String[] command = {"condor_status"};
        ProcessBuilder probuilder = new ProcessBuilder(command);
        Process process = probuilder.start();

        InputStream is = process.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String core_line;
        ArrayList<String>cores = new ArrayList<String>();
        System.out.printf("Output of running %s is:\n", Arrays.toString(command));
        while ((core_line = br.readLine()) != null) {
            if(core_line.contains("Total Owner Claimed Unclaimed Matched Preempting Backfill") || core_line.contains("X86_64/LINUX") || core_line.contains("Total")){
                System.out.println("core_line: " + core_line);
                cores.add(core_line);
            }
        }
    return cores;
    }
    
    
    /**
     * Method to kill all running jobs.
     * @throws IOException
     */
    public static void KillJobs() throws IOException {
        //command to kill all running jobs
        String[] command = {"condor_rm", "--all"};
        ProcessBuilder probuilder = new ProcessBuilder(command);

        Process process = probuilder.start();
        InputStream is = process.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line;
        System.out.printf("Output of running %s is:\n", Arrays.toString(command));
        line = br.readLine();
        System.out.println("Kiljobs line: " + line);
    }
}
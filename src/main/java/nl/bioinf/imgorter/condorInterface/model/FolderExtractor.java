/*
 * Copyright (c) 2017 Maaike Brummer & Iris Gorter
 * All rights reserved
 */
package nl.bioinf.imgorter.condorInterface.model;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;

/**
 * Class that gets all folders in a certain directory.
 */
public class FolderExtractor {

    public FolderExtractor() {

    }

    /**
     * Method that returns all folders in a certain directory.
     * @return directories
     */
    public static String[] getDatabaseFolder() {
        File file = new File("/data/datasets/");
        String[] directories = file.list(new FilenameFilter() {

            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });
        return directories;
    }
}

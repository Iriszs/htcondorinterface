/*
 * Copyright (c) 2017 Maaike Brummer & Iris Gorter
 * All rights reserved
 */
package nl.bioinf.imgorter.condorInterface.model;

import java.io.IOException;


/**
 *
 * This is the main class of the web application
 */
public class HTCondorInterface {

    HTCondorInterface() {

    }

    public static void main(String[] args) throws IOException, InterruptedException {
        //This main class makes a new condorExecuter object
        CondorExecuter c = new CondorExecuter();
        //and let it execute it's command
        c.executeCommands();
    }

}

/*
 * Copyright (c) 2017 Maaike Brummer & Iris Gorter
 * All rights reserved
 */
package nl.bioinf.imgorter.condorInterface.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * This class retrieves the username of the user by using the 'whoami' command
 * on the commandline
 * 
 */
public class UsernameGetter {
    
    public String getUsername() throws IOException, InterruptedException {
        //set commando whoami
        String[] command = {"whoami"};
        //execute command
        ProcessBuilder probuilder = new ProcessBuilder(command);
        Process process = probuilder.start();

        InputStream is = process.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line;
        System.out.printf("Output of running %s is:\n", Arrays.toString(command));
        while ((line = br.readLine()) != null) {
            System.out.println(line);
            return line;
        }
        //Wait to get exit value
        try {
            int exitValue = process.waitFor();
            System.out.println("\n\nExit Value is " + exitValue);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
     return "";
    }
    
    
    
    
}

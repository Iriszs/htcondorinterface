/*
 * Copyright (c) 2017 Maaike Brummer & Iris Gorter
 * All rights reserved
 */
package nl.bioinf.imgorter.condorInterface.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;


/**
 *
 * Class that executes condor with the chosen program.
 */
public class CondorExecuter {
    private Arguments arg = new Arguments();

    CondorExecuter() {
    }

    /**
     * @param newA 
     */
    public CondorExecuter(final Arguments newA) {
        arg = newA;
    }

    /**
     * This method executes 'condor_submit condor.cfg'.
     * it uses the generated configuration file that uses all the arguments from
     * the form.
     * @exception InterruptedException
     * this exception is thrown for the function that gets the username
     * @throws java.io.IOException
     * the io exception is used for the probuilder and bufferedreader, in case
     * the used file or directory is not accessible
     */
    public void executeCommands() throws IOException, InterruptedException {

        //set the command that needs to be executed on the commandline
        String[] command = {"condor_submit", "condor.cfg"};
        ProcessBuilder probuilder = new ProcessBuilder(command);
        //set outputpath for error files.
        UsernameGetter ug = new UsernameGetter();
        String username = ug.getUsername();
        String path = "/homes/userName/tmp/";

        //Change working directory to the one from the user
        probuilder.directory(new File(path.replace("userName", username)));
        Process process = probuilder.start();

        InputStream is = process.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line;
        System.out.printf("Output of running %s is:\n", Arrays.toString(command));
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }

        //Wait to get exit value
        try {
            int exitValue = process.waitFor();
            System.out.println("\n\nExit Value is " + exitValue);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
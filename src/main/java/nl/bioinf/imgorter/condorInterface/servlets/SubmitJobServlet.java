/*
 * Copyright (c) 2017 Maaike Brummer & Iris Gorter
 * All rights reserved
 */
package nl.bioinf.imgorter.condorInterface.servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nl.bioinf.imgorter.condorInterface.model.Arguments;
import nl.bioinf.imgorter.condorInterface.model.JobStartChecker;
import org.json.JSONObject;

/**
 *
 * This servlet is used to submit the form from a regular job
 */
@WebServlet(name = "SubmitJobServlet", urlPatterns = {"/submitJob.do"})
@MultipartConfig(location = "/homes/mlbrummer/tmp", maxFileSize = 10485760L)
public class SubmitJobServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String viewJsp = "Index.jsp";
        JSONObject jObject = new JSONObject(request.getParameter("formargs2"));

        String program = jObject.get("program").toString();
        String arguments = jObject.get("arguments").toString();
        String queue = jObject.get("queue").toString();
        String runname = jObject.get("runname").toString(); 

        Arguments arg = new Arguments();
        arg.setRunname(runname);
        arg.setExecutable(program);
        arg.setQueue(Integer.parseInt(queue));
        arg.setArguments(arguments);

        if (arg.getExecutablePathError() == 1) {
            System.out.println("error1");
            viewJsp = "programForm.jsp";
            session.setAttribute("ExecutablePathError", arg.getExecutablePathError());
        }
        String path = getServletContext().getRealPath("/WEB-INF/classes/demo.cfg");
        arg.setDemoFile(path);
        System.out.println("Path: " + path);

        JobStartChecker jsc = new JobStartChecker(arg);
        String errorFile = jsc.checkStart("REGULAR");
        session.setAttribute("errorFile", errorFile);

        if (jsc.getInputerror() == 0) {
            System.out.println("geen inputerror");
        } else if (jsc.getInputerror() == 1) {
            System.out.println("inputerror");
            session.setAttribute("inputerror", jsc.getInputerror());
            viewJsp = "form.jsp";
        }

        if (jsc.getIdleError() == 0) {
            System.out.println("geen idleError");
        } else if (jsc.getInputerror() == 1) {
            System.out.println("idleError");
            session.setAttribute("idleError", jsc.getIdleError());
            viewJsp = "form.jsp";
        }

        arg.setArguments(arguments);
        RequestDispatcher view = request.getRequestDispatcher(viewJsp);
        view.forward(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    } // </editor-fold>

}

/*
 * Copyright (c) 2017 Maaike Brummer & Iris Gorter
 * All rights reserved
 */
package nl.bioinf.imgorter.condorInterface.servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nl.bioinf.imgorter.condorInterface.model.Arguments;
import nl.bioinf.imgorter.condorInterface.model.JobStartChecker;
import org.json.JSONArray;
import org.json.JSONObject;


/**
 * This servlet is used to submit the values that are entered in the form
 * for a new blast run
 */
@WebServlet(name = "SubmitServlet", urlPatterns = {"/submitBlast.do"})
@MultipartConfig(location = "/homes/mlbrummer/tmp", maxFileSize = 10485760L)
public class SubmitServlet extends HttpServlet {
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //RequestDispatcher view = request.getRequestDispatcher("index.html");
        //view.forward(request, response);
        HttpSession session = request.getSession();
        String viewJsp = "Index.jsp";

        // JSON
        System.out.println("Argumenten: " + request.getParameter("formargs"));
        JSONObject jObject  = new JSONObject(request.getParameter("formargs"));

        JSONArray ids = jObject.getJSONArray("inputfile");

        String program = jObject.get("program").toString();
        String inputfilepath = jObject.get("inputfilepath").toString();
        String outputfile = jObject.get("outputfile").toString();
        String queue = jObject.get("queue").toString();
        String db = jObject.get("database").toString();
        String database = db + '/' + db;
        System.out.println("database correct" + database);
        String runname = jObject.get("runname").toString();
        

        String f = "";
        for (int i = 0; i < ids.length(); i++) {
            String file = ids.getString(i).toString();
            file = inputfilepath + file + " ";
            f = f + file;
        }
        System.out.println("f: " + f);

        Arguments arg = new Arguments();
        arg.setExecutable(program);
        arg.setQueue(Integer.parseInt(queue));
        arg.setQuery(f);
        arg.setOut(outputfile);
        arg.setDb(database);
        arg.setRunname(runname);

        if (arg.getOutputPathError() == 0) {
            System.out.println("error 0");
        } else if (arg.getOutputPathError() == 1) {
            System.out.println("error 1");
            session.setAttribute("OutputPathError", arg.getOutputPathError());
            viewJsp = "form.jsp";
        }

        String path = getServletContext().getRealPath("/WEB-INF/classes/demo.cfg");
        arg.setDemoFile(path);
        System.out.println("Path: " + path);

        JobStartChecker jsc = new JobStartChecker(arg);
        String errorFile = jsc.checkStart("BLAST");
        session.setAttribute("errorFile", errorFile);

        if (jsc.getInputerror() == 0) {
            System.out.println("geen inputerror");
        } else if (jsc.getInputerror() == 1) {
            System.out.println("inputerror");
            session.setAttribute("inputerror", jsc.getInputerror());
            viewJsp = "form.jsp";
        }

        if (jsc.getIdleError() == 0) {
            System.out.println("geen idleError");
        } else if (jsc.getInputerror() == 1) {
            System.out.println("idleError");
            session.setAttribute("idleError", jsc.getIdleError());
            viewJsp = "form.jsp";
        }

        RequestDispatcher view = request.getRequestDispatcher(viewJsp);
        view.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

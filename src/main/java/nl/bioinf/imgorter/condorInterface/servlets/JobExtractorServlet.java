/*
 * Copyright (c) 2017 Maaike Brummer & Iris Gorter
 * All rights reserved
 */
package nl.bioinf.imgorter.condorInterface.servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.bioinf.imgorter.condorInterface.model.JobExtractor;

/**
 * Servlet that is used to extract the number of jobs that needs to be displayed
 * on the home page
 */
@WebServlet(name = "JobExtractorServlet", urlPatterns = {"/JobExtractor.do"})
public class JobExtractorServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        JobExtractor je = new JobExtractor();

        RequestDispatcher view = request.getRequestDispatcher("Index.jsp");
        view.forward(request, response);
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    } // </editor-fold>

}

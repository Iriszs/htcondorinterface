# README #

This is a Interface for the condor grid on the bioinf network.

### What is this repository for? ###

* Handing in the Project
* Version

### How do I get set up? ###

* Download this project.
* Go to the directory condorinterface/target in the terminal.
* Type: java8 -jar condorInterface-1.0-jar-with-dependencies.jar
* Tomcat is running now, type in your browser: "http://localhost:8081/condorInterface/Index.jsp" now you can use the program.

### Who do I talk to? ###

* Iris Gorter
* Maaike Brummer